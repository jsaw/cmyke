# My/Init - Initialization

This module initializes CMyke and loads the users `User/Init`.

Use the following two lines

	find_package(CMyke)
	include(My/Init OPTIONAL)

before the ``project`` command.

Note, that the keyword ``OPTIONAL`` is essential in case /CMyke/ is not
installed on the target system so that the project will configure and build
without.

If `CMyke` is present, discovered settings are recorded in a report file
(use ``MY_REPORT=My cmake .`` from your build directory).

The author uses the following ``User/Init`` (see MY_USER_PATHS in the report
file):

	message(NOTICE "Using User/Init")
	list(APPEND CMAKE_MODULE_PATH "$ENV{HOME}/build/.settings")
	include(Project/Init)


**See also**:  
[My/Platform](Platform.md)  
[My/Config](Config.md)  
[My/Lists](Lists.md)  
[My/Report](Report.md)
