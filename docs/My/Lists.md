# My/Lists - Personalized configuration.

This file should be included at the end of the `CMakeLists.txt` file:

	include(My/Lists OPTIONAL)

In addition to loading `User/Lists`, a report about CMyke's settings is generated.

**See also**:  
[My/Init](Init.md)  
[My/Config](Config.md)  
[My/Report](Report.md)
