# My/Config - Project configuration settings

This file should be included directly after the ``project`` command in the
`CMakeLists.txt` file:

	include(My/Config OPTIONAL)

**See also**:  
[My/Init](Init.md)  
[My/Lists](Lists.md)
