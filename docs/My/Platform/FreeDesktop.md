# My/Platform/FreeDesktop

Platform settings if host is following Free Desktop specifications.

**See**:
- [My/Platform/Common/Unix](Common/Unix.md)
