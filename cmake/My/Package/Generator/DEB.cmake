# Distributed under the OSI-approved MIT License. See accompanying
# file LICENSE or https://bitbucket.org/jsaw/cmyke/src/master/LICENSE for details.

#[=======================================================================[.md:
# My/Package/Generator/DEB

**See also**:
- [CPack DEB Generator](https://cmake.org/cmake/help/latest/cpack_gen/deb.html)
#]=======================================================================]
include_guard(GLOBAL)

include(My/Platform/Utils/Unix)

message(TRACE "Loaded My/Package/Generator/DEB...")

#[==[.md:
# my_generator_deb

	FIXME

#]==]
function(my_generator_deb)
	message(TRACE "my_generator_deb(${__MY_PACKAGE_ARGS})")
	list(APPEND CMAKE_MESSAGE_INDENT "	")

	### parse options
	# COMMON?
	my_generator_iscommon(COMMON)

	if(COMMON)
		set(__my_generator_prefix MY_DEB_COMMON)
	else()
		set(__my_generator_prefix MY_DEB)
	endif()

	# parse arguments
	my_options_parse(${__my_generator_prefix} RESET NODEFAULTS
		OPTIONS __MY_PACKAGE_COMMON__
		${__MY_PACKAGE_ARGS}
	)
	set(__MY_PACKAGE_ARGS ${${__my_generator_prefix}_UNPARSED_ARGUMENTS})

	my_options_parse(${__my_generator_prefix} RESET
		OPTIONS __MY_PACKAGE_DEB__ {
			TARGET:

			DISTRIBUTION:=Debian
			CODENAME:
			VERSION:
			ARCHITECTURE:=${MY_PACKAGE_COMMON_ARCHITECTURE}

			PREDEPENDS:*
			DEPENDS:*
			ENHANCES:*
			BREAKS:*
			CONFLICTS:*
			PROVIDES:*
			REPLACES:*
			RECOMMENDS:*
			SUGGESTS:*
			MAINTAINER:
			RELEASE:
			SOURCES:{
				PREFIX:=/usr/src
			}
			PRIORITY:
			SHLIBDEPS:
		}
		${__MY_PACKAGE_ARGS}
	)

	if(${__my_generator_prefix}_UNPARSED_ARGUMENTS)
		message(FATAL_ERROR "Unrecognized arguments passed to my_generator_archive: ${__MY_PACKAGE_ARGS}")
	endif()

	# handle COMMON
	if(COMMON)
		# promote to parent scope
		get_cmake_property(allvars VARIABLES)
		foreach(var ${allvars})
			if(var MATCHES "^${__my_generator_prefix}")
				set(${var} ${${var}} PARENT_SCOPE)
			endif()
		endforeach()

		# done
		return()
	endif()

	# check distribution/version/codename
	set(DEBCONFIG Deb)
	set(DEBTARGET deb)
	my_package_unix_sysinfo(DISTRONAME DISTROVERSION CODENAME DISTROARCH)
	if(NOT "${MY_DEB_DISTRIBUTION}" STREQUAL "${DISTRONAME}")
		message(DEBUG "Skipping (distribution mismatch)...")
		return()
	else()
		set(DEBCONFIG ${DEBCONFIG}${DISTRONAME})
		set(DEBTARGET ${DEBTARGET}-${DISTRONAME})
	endif()
	if(MY_DEB_VERSION)
		if(NOT "${MY_DEB_VERSION}" STREQUAL "${DISTROVERSION}")
			message(DEBUG "Skipping (version mismatch)...")
			return()
		else()
			set(DEBCONFIG ${DEBCONFIG}${DISTROVERSION})
			set(DEBTARGET ${DEBTARGET}-${DISTROVERSION})
		endif()
	endif()
	if(MY_DEB_CODENAME)
		if(NOT "${MY_DEB_CODENAME}" STREQUAL "${CODENAME}")
			message(DEBUG "Skipping (codename mismatch)...")
			return()
		else()
			set(DEBCONFIG ${DEBCONFIG}${CODENAME})
			set(DEBTARGET ${DEBTARGET}-${CODENAME})
		endif()
	endif()

	if(NOT "${MY_DEB_ARCHITECTURE}" STREQUAL "all")
		if(NOT "${MY_DEB_ARCHITECTURE}" STREQUAL "${DISTROARCH}")
			message(DEBUG "Skipping (architecture mismatch)...")
			return()
		else()
			set(DEBCONFIG ${DEBCONFIG}${DISTROARCH})
			set(DEBTARGET ${DEBTARGET}-${DISTROARCH})
		endif()
	endif()

	# default target name
	string(TOLOWER "${DEBTARGET}" DEBTARGET)
	if(NOT MY_DEB_TARGET)
		string(TOLOWER "${DEBTARGET}" MY_DEB_TARGET)
	endif()

	### generate
	# prepare
	set(MY_DEB_GENERATOR DEB)
	my_generator_reset()
	my_generator_config(
		VARIABLES MY_DEB MY_DEB_COMMON
		TEMPLATES NAME VERSION VENDOR SUFFIX
	)

	# setup config file name
	my_generator_config(GET CONFIG CONFIG)
	if(NOT CONFIG)
		set(MY_DEB_CONFIG CPack${DEBCONFIG}Config.cmake)
	endif()

	my_generator_config(GET CONFIG CONFIG)
	my_target(package-${MY_DEB_TARGET}
		COMMENT "Create deb package for ${DISTRONAME} (${CONFIG})"
		COMMAND ${CMAKE_CPACK_COMMAND} --config ${MY_DEB_CONFIG}
		WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
	)

	# check components
	my_generator_config(GET COMPONENTS COMPONENTS)
	my_generator_config(GET NOCOMPONENTS NOCOMPONENTS)

	if(NOCOMPONENTS)
		unset(COMPONENTS)
	endif()

	# handle components
	if(COMPONENTS)
		my_components("" ${COMPONENTS})
		set(CPACK_DEB_COMPONENT_INSTALL ON)
	endif()

	# populate CPACK_* variables
	my_generator_config(
		POPULATE CPACK
			OUTPUT_CONFIG_FILE=CONFIG
			GENERATOR
	)

	# filename
	my_generator_config(GET PACKAGE_NAME NAME)
	my_generator_config(GET PACKAGE_VERSION VERSION)
	set(MY_DEB_FILE_NAME_BASE "${PACKAGE_NAME}-${PACKAGE_VERSION}")
	string(TOLOWER "${MY_DEB_FILE_NAME_BASE}" MY_DEB_FILE_NAME_BASE)

	set(MY_DEB_FILE_NAME "${MY_DEB_FILE_NAME_BASE}")
	my_generator_config(
		POPULATE CPACK_PACKAGE
			FILE_NAME
			DESCRIPTION_SUMMARY
	)


# CPACK_DEBIAN_COMPRESSION_TYPE
# CPACK_DEBIAN_ENABLE_COMPONENT_DEPENDS
# CPACK_DEBIAN_PACKAGE_GENERATE_SHLIBS
# CPACK_DEBIAN_PACKAGE_GENERATE_SHLIBS_POLICY

# CPACK_DEBIAN_PACKAGE_CONTROL_EXTRA
# CPACK_DEBIAN_PACKAGE_CONTROL_STRICT_PERMISSION

# CPACK_DEBIAN_DEBUGINFO_PACKAGE
# CPACK_DEBIAN_PACKAGE_SOURCE

	my_generator_config(
		POPULATE CPACK_DEBIAN_PACKAGE
			NAME
			EPOCH
			VERSION
			RELEASE
			ARCHITECTURE
			PREDEPENDS
			DEPENDS
			ENHANCES
			BREAKS
			CONFLICTS
			PROVIDES
			REPLACES
			RECOMMENDS
			SUGGESTS
			MAINTAINER
			DESCRIPTION=DESCRIPTION_FULL
			SECTION=CATEGORY
			PRIORITY
			SHLIBDEPS
	)

	# populate CPACK_SOURCE_* variables
	string(REGEX REPLACE "Config[.]cmake$" "SourceConfig.cmake" MY_DEB_SOURCE_CONFIG ${MY_DEB_CONFIG})
	my_target(package-${MY_DEB_TARGET}-source
		COMMENT "Create deb package for ${DISTRONAME} (${MY_DEB_CONFIG})"
		COMMAND ${CMAKE_CPACK_COMMAND} --config ${MY_DEB_CONFIG}
		WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
	)

	my_generator_config(
		VARIABLES MY_DEB_SOURCE MY_DEB_COMMON_SOURCE MY_DEB MY_DEB_COMMON MY_PACKAGE_COMMON_SOURCE
	)

	my_generator_config(
		POPULATE CPACK_SOURCE
			GENERATOR
			OUTPUT_CONFIG_FILE=CONFIG
			IGNORE_FILES
			STRIP_FILES
	)

	set(MY_DEB_FILE_NAME "${MY_DEB_FILE_NAME_BASE}-source")
	my_generator_config(
		POPULATE CPACK_SOURCE_PACKAGE
			FILE_NAME
	)

	# create CPack config
	my_generator_emit()

if(false)
	get_cmake_property(allvars VARIABLES)
	foreach(var ${allvars})
		if(var MATCHES "^CPACK")
			message("${var}=${${var}}")
		endif()
	endforeach()
endif()
	list(POP_BACK CMAKE_MESSAGE_INDENT)
endfunction()

