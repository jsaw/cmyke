# Distributed under the OSI-approved MIT License. See accompanying
# file LICENSE or https://bitbucket.org/jsaw/cmyke/src/master/LICENSE for details.

#[=======================================================================[.md:
# My/Bits - Tools and utilities.

**See**:\\
[My/Bits/Auxiliary](Bits/Auxiliary.md)\\
[My/Bits/Options](Bits/Options.md)\\
[My/Bits/Set](Bits/Set.md)\\
[My/Bits/String](Bits/String.md)

#]=======================================================================]
include_guard(GLOBAL)

include(My/Bits/Auxiliary)
include(My/Bits/Options)
include(My/Bits/Set)
include(My/Bits/List)
include(My/Bits/String)
