# Distributed under the OSI-approved MIT License. See accompanying
# file LICENSE or https://bitbucket.org/jsaw/cmyke/src/master/LICENSE for details.

my_report(My/Platform %{BR} "Loaded platform specific settings: 'Linux'.")

if(DEFINED ENV{XDG_DATA_DIRS})
	include(My/Platform/FreeDesktop)
else()
	include(My/Platform/Unix)
endif()

