# TODO

## General

- documentation should wrap around column 80
- sort installation components

## External

- handle TAINTED
- implement DIRECTORY and TRANSFER methods
- complete GIT module
- add TARGET property
- document 'User/External' better
- add 'sites' to MY_REPORT

## Init/Lists

- handle TOOLCHAIN/cross-compilation (CMAKE_CROSSCOMPILING)
- check on Windows
- check on Mac

## Package

- for packaging targets, there should be an option to add TARGET DEPENDS
  (e.g. with CMyke, update-source-docs should be run prior to packaging)

### DEB (and others)

- need to install sources by default in /usr/src
- SHLIBDEPS auto-detect?

## Documentation

- complete documentation
- create an API index

